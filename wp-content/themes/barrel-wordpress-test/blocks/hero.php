<?php
  $featured_post = get_field( 'post' );
  the_module( 'hero', array(
    'background_image'  => get_field('background_image'),
    'headline_text'     => get_field('headline_text'),
    'main_text'         => get_field('main_text'),
    'foreground_image'  => get_field('foreground_image'),
    'featured_post'     => $featured_post,
  ) );
