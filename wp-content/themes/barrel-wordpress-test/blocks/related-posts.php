<div class="layout" data-module="layout">
  <div class="container">
    <h2 class="layout__title"><?php _e('Related Articles', 'barrel-wordpress-test'); ?></h2>

    <?php
    $args = array( 'offset' => '1' );
    if ( $is_preview ) {
      $args['posts_per_page'] = '3';
    }
    $recent = new WP_Query( $args );

    while ( $recent->have_posts() ) {

      $recent->the_post();

      the_module('post');

    }
    wp_reset_postdata(); ?>
  </div>
</div>
