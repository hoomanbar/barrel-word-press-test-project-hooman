<?php
/**
 * Hero (and two-up) block
 * @author BarrelNY
 */

$foreground_image_url = esc_url($foreground_image['url']);
$background_image_url = esc_url($background_image['url']);

?>
<section class="hero">
  <div class="container hero__main" style="background-image:url('<?php echo $background_image_url; ?>');">

    <div class="hero__main--content">
      <h1><?php echo $headline_text; ?></h1>
      <p><?php echo $main_text; ?></p>
    </div>

    <div class="hero__main--image">
      <span style="background-image:url('<?php echo $foreground_image_url; ?>')">
      </span>
    </div>

  </div>
  <div class="container two-up">
    <?php
      $latest = new WP_Query( 'posts_per_page=1' );

      while ( $latest->have_posts() ) {

        $latest->the_post();

        the_module('post', array('type' => 'two-up'));

      }

      wp_reset_postdata();
    ?>
  </div>
</section>
